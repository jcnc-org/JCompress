# JCompress

![](https://img.shields.io/badge/Windows-Passing-49%2C198%2C84.svg?style=falt&logo=Windows)
![](https://img.shields.io/badge/Ubuntu-Passing-49%2C198%2C84.svg?style=falt&logo=Ubuntu)
![](https://img.shields.io/badge/MacOS-Passing-49%2C198%2C84.svg?style=falt&logo=Apple)

QQ群：386279455

#### 软件运行截图
- Windows 平台
  ![windows](screenshot/windows-1.png)
  ![windows](screenshot/windows-2.png)


#### 安装教程

1.  Windows 平台，可以直接使用我编译好的可执行程序或自己编译

[gitee-download]: https://gitee.com/jcnc-org/JCompress/releases
- [下载][gitee-download]

2. Linux/MacOS 平台，克隆后自行编译

<pre><code>git clone https://gitee.com/jcnc-org/JCompress.git</code></pre>



#### 参与贡献

1. Fork 本仓库
2. 加入JCNC社区
3. 新建分支
4. 提交代码
5. 新建 Pull Request
