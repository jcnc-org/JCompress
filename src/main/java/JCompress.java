import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.File;
import java.io.IOException;

public class JCompress extends Application {

    String iconSrc = "img/icon.png";
    String title = "JCompress";
    private ZipCompress zipper;

    String fileName = "";

    ProgressBar progressBar = new ProgressBar(0);

    @Override
    public void start(Stage primaryStage) throws Exception {
        zipper = new ZipCompress();

        BorderPane root = new BorderPane();
        Scene scene = new Scene(root, 400, 200);

        GridPane gridPane = new GridPane();
        gridPane.setPadding(new Insets(10, 10, 10, 10));
        gridPane.setVgap(5);
        gridPane.setHgap(5);

        //选择文件
        Label fileLabel = new Label("选择要压缩的文件:");
        TextField fileField = new TextField();
        Button fileButton = new Button("浏览");
        fileButton.setOnAction(e -> {
            File selectedFile = chooseFile(primaryStage);
            if (selectedFile != null) {
                fileField.setText(selectedFile.getAbsolutePath());
            }
        });
        gridPane.add(fileLabel, 0, 0);
        gridPane.add(fileField, 1, 0);
        gridPane.add(fileButton, 2, 0);

        //选择文件夹
        Label folderLabel = new Label("选择要压缩的文件夹:");
        TextField folderField = new TextField();
        Button folderButton = new Button("浏览");
        folderButton.setOnAction(e -> {
            File selectedDirectory = chooseDirectory(primaryStage);
            if (selectedDirectory != null) {
                folderField.setText(selectedDirectory.getAbsolutePath());
            }
        });

        gridPane.add(folderLabel, 0, 1);
        gridPane.add(folderField, 1, 1);
        gridPane.add(folderButton, 2, 1);


        //选择保存位置和名称
        Label outputLabel = new Label("选择保存的位置和文件名:");
        TextField outputField = new TextField();
        Button outputButton = new Button("浏览");
        outputButton.setOnAction(e -> {
            File selectedDirectory = chooseDirectory(primaryStage);
            if (selectedDirectory != null) {

                if (!fileField.getText().isEmpty()) {
                    fileName = new File(fileField.getText()).getName();
                } else if (!folderField.getText().isEmpty()) {
                    fileName = new File(folderField.getText()).getName();
                }
                outputField.setText(selectedDirectory.getAbsolutePath() + File.separator + fileName + ".zip");
            }
        });
        gridPane.add(outputLabel, 0, 2);
        gridPane.add(outputField, 1, 2);
        gridPane.add(outputButton, 2, 2);


        //压缩按钮
        Button compressButton = new Button("压缩");
        compressButton.setOnAction(e -> {
            MyTimer timer = new MyTimer();
            try {
                if (!fileField.getText().isEmpty()) {
                    zipper.zipFiles(fileField.getText(), outputField.getText());
                    alertCompress(timer);

                } else if (!folderField.getText().isEmpty()) {
                    zipper.zipFiles(folderField.getText(), outputField.getText());
                    alertCompress(timer);
                }
            } catch (IOException ex) {
                ex.printStackTrace();
                alertNoneCompress();
            }
        });
        gridPane.add(compressButton, 1, 3);

        HBox primaryHBox = new HBox();
        primaryHBox.getChildren().add(gridPane);
        primaryHBox.setAlignment(Pos.CENTER);

        VBox primaryVBox = new VBox();
        primaryVBox.getChildren().add(primaryHBox);
        primaryVBox.setAlignment(Pos.CENTER);

        root.setCenter(primaryVBox);
        primaryStage.setScene(scene);
        primaryStage.setTitle("JCompress");
        primaryStage.getIcons().add(new Image(iconSrc));
        primaryStage.setTitle(title);
        primaryStage.show();
    }

    private void alertCompress(MyTimer timer) {
        timer.stop();
        //创建一个成功的提示框并显示
        Alert alertCompressButton = new Alert(Alert.AlertType.INFORMATION);
        alertCompressButton.setTitle("提示");
        alertCompressButton.setHeaderText(null);
        alertCompressButton.setContentText("压缩成功！\n\n文件名:" + fileName + ".zip" + "\n\n用时:" + timer.getSecond() + "秒");
        Stage stage = (Stage) alertCompressButton.getDialogPane().getScene().getWindow();
        stage.getIcons().add(new Image(iconSrc));
        alertCompressButton.showAndWait();


    }

    private void alertNoneCompress() {
        //创建一个成功的提示框并显示
        Alert alertCompressButton = new Alert(Alert.AlertType.INFORMATION);
        alertCompressButton.setTitle("提示");
        alertCompressButton.setHeaderText(null);
        alertCompressButton.setContentText("请选择于压缩文件和压缩路径！");
        Stage stage = (Stage) alertCompressButton.getDialogPane().getScene().getWindow();
        stage.getIcons().add(new Image(iconSrc));
        alertCompressButton.showAndWait();


    }

    private File chooseFile(Stage stage) {
        FileChooser fileChooser = new FileChooser();
        return fileChooser.showOpenDialog(stage);
    }

    private File chooseDirectory(Stage stage) {
        DirectoryChooser directoryChooser = new DirectoryChooser();
        return directoryChooser.showDialog(stage);
    }

    public static void main(String[] args) {
        launch(args);
    }
}



